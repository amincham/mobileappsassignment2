package uni16003689.addison.assignmenttwo_addisonmincham;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.widget.ImageView;

public class OnePhoto extends AppCompatActivity {
    private ScaleGestureDetector mScaleGestureDetector;
    private float Scale = 1.0f;
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Set Layout
        setContentView(R.layout.image);
        //get the intent
        Intent intent = getIntent();
        ///Get the correct image to view
        Uri image = Uri.parse(intent.getStringExtra("Position"));
        //Set its orientation
        int orientation = intent.getIntExtra("Orientation", 0);
        imageView=findViewById(R.id.img);
        imageView.setImageURI(image);
        imageView.setRotation(orientation);
        mScaleGestureDetector = new ScaleGestureDetector(this, new ScaleListener());
    }
    @Override
    //OnTouchEvent for pinch and zoom
    public boolean onTouchEvent(MotionEvent motionEvent) {
        mScaleGestureDetector.onTouchEvent(motionEvent);
        return true;
    }

    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        @Override
        //Pinch and Zoom implementation
        public boolean onScale(ScaleGestureDetector scaleGestureDetector){
            Scale *= scaleGestureDetector.getScaleFactor();
            Scale = Math.max(0.1f,
                    Math.min(Scale, 10.0f));
            imageView.setScaleX(Scale);
            imageView.setScaleY(Scale);
            return true;
        }
    }
}
