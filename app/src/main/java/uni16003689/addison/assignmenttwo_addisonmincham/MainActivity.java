package uni16003689.addison.assignmenttwo_addisonmincham;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.Image;
import android.media.ThumbnailUtils;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.LruCache;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;

public class MainActivity extends AppCompatActivity {
    //Variables
    ImageAdapter adapter;
    GridView gridView;
    //Variable for Cache
    private LruCache<String, Bitmap> Cache;

    //
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Permissions for Reading External Storage on Device
        setContentView(R.layout.activity_main);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                ActivityCompat.requestPermissions(this, new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            }
        }
            //Sorting out for Cache
            final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
            final int cacheSize = maxMemory / 8;
            Cache = new LruCache<String, Bitmap>(cacheSize) {
                @Override
                protected int sizeOf(String key, Bitmap bitmap) {

                    return bitmap.getByteCount() / 1024;
                }
            };
            //Finding the appropriate id for each variable
            gridView = findViewById(R.id.gridView);

            adapter = new ImageAdapter();

            gridView.setAdapter(adapter);

            //Setting the onClick Listener for displaying one photo on a separate activity
            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String path = getImagesURI().get(position);
                    int ori = getOrientation().get(position);
                    Intent intent = new Intent(MainActivity.this, OnePhoto.class);
                    intent.putExtra("Position", path);
                    intent.putExtra("Orientation", ori);
                    startActivity(intent);
                }
            });
    }

    //Toasts for the completed Permissions
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case 1:{
                if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    if(ContextCompat.checkSelfPermission(this,Manifest.permission.READ_EXTERNAL_STORAGE)==PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(this, "Yay, Permission Granted!", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(this, "Ding Dong, You did it wrong", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }
            }
        }
    }

    //Class for the main Image Processing
    public class ImageAdapter extends BaseAdapter {
        private BitmapFactory.Options pixel = new BitmapFactory.Options();


        ImageAdapter() {
            super();
            pixel.inSampleSize = 4;
        }
        class ViewHolder {
            //Variables for the position of photos in the array, the img itself and the uriPath for the photo
            int position;
            ImageView img;
            String uriPath;
        }




        @Override
        //Setting the Count to the Size of the Array so it doesn't try and load more pictures than there actually is.
        //If it would do this it would crash the app
        public int getCount() {
            return getImagesURI().size() ;
        }
        @Override
        public Object getItem(int i) {
            return null;
        }
        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(final int pos, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;

            if(convertView == null){
                //Inflate from xml if convertView is null
                convertView = getLayoutInflater().inflate(R.layout.image,parent,false);
                //Creating a new ViewHolder
                viewHolder = new ViewHolder();
                viewHolder.img=convertView.findViewById(R.id.img);
                //Setting Tag
                convertView.setTag(viewHolder);

            }else {
                //If its not null get the viewHolder
                viewHolder = (ViewHolder) convertView.getTag();
            }
            //Set position
            viewHolder.position=pos;
            //Erase photos from previous times
            viewHolder.img.setImageBitmap(null);
            viewHolder.uriPath = getImagesURI().get(pos);
            //load images
            new AsyncTask<ViewHolder,Void,Bitmap>(){
                private ViewHolder viewHolder;
                @Override
                protected Bitmap doInBackground(ViewHolder... params) {
                    viewHolder = params[0];
                    Bitmap bit = null;
                    try{
                        if (viewHolder.position != pos){
                            return null;
                        }
                        //If the image is already cached return the cached image
                        Bitmap CacheImage = getBitmapFromMemCache(viewHolder.uriPath);
                        if(CacheImage!=null){
                            return CacheImage;

                        }else{
                            //Make the Thumbnail and add it to the cache
                            bit = ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(viewHolder.uriPath,pixel), viewHolder.img.getWidth(),viewHolder.img.getHeight());
                            addBitmapToMemoryCache(viewHolder.uriPath,bit);

                        }

                    }catch (Exception except){
                        //Throw Exception
                        except.printStackTrace();
                    }
                    return bit;


                }

                @Override
                protected void onPostExecute(Bitmap bit){
                    //set imageView if it has not changed
                    if (viewHolder.position==pos){
                        viewHolder.img.setImageBitmap(bit);
                        viewHolder.img.setRotation(getOrientation().get(pos));
                    }
                }


            }.execute(viewHolder);
            return convertView;
        }

    }

    //Setting the orientation for each photo so it is the right way up.
    public ArrayList<Integer> getOrientation(){
        ArrayList<Integer> finalOrientation = new ArrayList<>();
        String orderBy = MediaStore.Images.Media.DATE_ADDED + " DESC";


        String[] columns = { MediaStore.Images.Media.ORIENTATION};
        Cursor cursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,columns,null,null,orderBy);
        int number = cursor.getCount();
        ArrayList<Integer> path = new ArrayList<>();
        for(int i = 0; i<number;i++){
            cursor.moveToPosition(i);
            int rotateindex = cursor.getColumnIndex(MediaStore.Images.Media.ORIENTATION);

            path.add(cursor.getInt(rotateindex));

            finalOrientation.add(path.get(i));

        }

        return  finalOrientation;
    }
    //Getting the Array of uri's of images and passing it back to be processed
    public ArrayList<String> getImagesURI(){
        ArrayList<String> finalPathURIs = new ArrayList<>();
        String orderBy = MediaStore.Images.Media.DATE_ADDED + " DESC";


        String[] columns = { MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,columns,null,null,orderBy);
        int number = cursor.getCount();
        ArrayList<String> path = new ArrayList<>();
        for(int i = 0; i<number;i++){
            cursor.moveToPosition(i);
            int pathindex = cursor.getColumnIndex(MediaStore.Images.Media.DATA);

            path.add(cursor.getString(pathindex));

            finalPathURIs.add(path.get(i));

        }

        return  finalPathURIs;
    }

    //If the IMage isnt in the Cache, add it to the cache
    public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (getBitmapFromMemCache(key) == null) {
            Cache.put(key, bitmap);
        }
    }

    //Returns the image from the cache
    public Bitmap getBitmapFromMemCache(String key) {
        return Cache.get(key);
    }







}
